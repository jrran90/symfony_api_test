<?php namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
	public function __construct()
	{
		// Created an array since I was having problem with installing pdo_mysql
		$this->user = [
			[
				'id' => 1,
				'name' => 'jhon',
				'email' => 'jr.ran90@gmail.com'
			],
			[
				'id' => 2,
				'name' => 'rey',
				'email' => 'email@email.com'
			],
			[
				'id' => 3,
				'name' => 'ranario',
				'email' => 'mail@example.com'
			]
		];
	}


	/**
	 * @Route("/")
	 */
	public function index()
	{
		return $this->render('users/index.html.twig', ['users' => $this->user]);
	}

	/**
	 * @Route("/api/user")
	 * @Method("GET")
	 */
	public function indexUser()
	{
		return new JsonResponse($this->user);
	}

	/**
	 * @Route("/api/user/{id}")
	 * @Method("GET")
	 */
	public function showUser($id)
	{
		$user = [];

		foreach ($this->user as $k => $value) {
			if ($value["id"]==$id) {
				$user = $value;
			}
		}

		return new JsonResponse($user);
	}

	/**
	 * @Route("/user/create")
	 */
	public function createUser()
	{
		return $this->render('users/create.html.twig');
	}


	/**
	 * @Route("/api/user")
	 * @Method("POST")
	 */
	public function postUser(Request $request)
	{
		if ($request->isMethod('POST')) {
			// should save to database
			return new JsonResponse(['name' => $request->get('name')]);
		}
	}

	/**
	 * This will just load a form in which the user can edit and save to database
	 * @Route("/user/{id}/edit")
	 */
	public function editUser($id)
	{
		$user = [];

		foreach ($this->user as $k => $value) {
			if ($value["id"]==$id) {
				$user = $value;
			}
		}

		return $this->render('users/edit.html.twig', ['users' => $user]);
	}

	/**
	 * @Route("/api/user/{id}")
	 * @Method("PUT")
	 */
	public function updateUser($id)
	{
		$user = [];

		foreach ($this->user as $k => $value) {
			if ($value["id"]==$id) {
				$user = $value;
			}
		}

		return $this->render('users/edit.html.twig', ['users' => $user]);
	}

	/**
	 * @Route("/api/user/{id}")
	 * @Method("DELETE")
	 */
	public function destroyUser($id)
	{
		return new JsonResponse($id);
	}

}